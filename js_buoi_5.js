// Bai 1: Tinh Diem Thi
function inKetQuaThi(){
   var diem1 =  document.getElementById("diem-1").value * 1; 
   var diem2 =  document.getElementById("diem-2").value * 1; 
   var diem3 =  document.getElementById("diem-3").value * 1;
   var diemChuan = document.getElementById("diem-chuan").value * 1;
   var diemKhuVuc = document.getElementById("khu-vuc").value * 1;
   var diemDoiTuong = document.getElementById("doi-tuong").value * 1;
   var diemTongKet = diem1 + diem2 + diem3 + diemKhuVuc + diemDoiTuong;
   var ketQua = "";
   console.log("Diem tong ket: ", diemTongKet);
   if( diemTongKet >= diemChuan){
    ketQua = "Đậu"
    console.log("Ket Qua:", ketQua);
   } else{
    ketQua = "Trượt"
    console.log("Ket Qua:", ketQua);
   }
   document.getElementById("bai-1-result").innerHTML = `<p>Tổng Điểm: ${diemTongKet} <br> Kết Quả: ${ketQua} </p>`
}

// Bai 2: Tinh Tien Dien

function tinhTienDien(){
    var dienTieuThu = document.getElementById("dien-tieu-thu").value * 1;
    var tienDien = 0;
    if (dienTieuThu <= 50){
        tienDien = dienTieuThu * 500;
    } else if( dienTieuThu<=100){
        tienDien = 50 * 500 + (dienTieuThu - 50)* 650;
    } else if( dienTieuThu<=200){
        tienDien = 50 * 500 + 50 * 650 + (dienTieuThu - 100)* 850;
    } else if( dienTieuThu<=350){
        tienDien = 50 * 500 + 50 * 650 + 100 * 850 + (dienTieuThu - 200)* 1100;
    }  else {
        tienDien = 50 * 500 + 50 * 650 + 100 * 850 + 150 * 1100 + (dienTieuThu - 350)* 1300;
    }
    tienDien = tienDien.toLocaleString('en-US');
    console.log("Tong Tien Dien: ", tienDien);
    document.getElementById("bai-2-result").value = `${tienDien} VNĐ`;
}